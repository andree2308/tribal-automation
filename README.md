# TRIBAL - MANUAL TESTING & AUTOMATION PROJECT


## AUTOMATION PROJECT


### FEATURES
find_current_job_openings.feature

### STEPS
find_current_job_openings_steps.py

### EXECUTION
To execute the feature scenarios run the command:

behave features/find_current_job_openings.feature

To run a specific scenario (test01 and test02) you should run the commands:

behave --no-capture --tags=@test-01

behave --no-capture --tags=@test-02

## MANUAL TESTING


###SECTION 1. UI MANUAL TESTING
This spreadsheet contains the test cases for the manual testing of the Login Page 

PATH:
/tribal-automation/manual_testing/api_manual_testing


###SECTION 2. API MANUAL TESTING
This spreadsheet contains the test cases of the API testing:
- Create pet information
- Update pet information
- Retrieve pet information

PATH:
/tribal-automation/manual_testing/ui_manual_testing




from behave import *

import time


use_step_matcher("re")


@given("I am on the Tribal Credit Website")
def step_impl(context):
    context.helperfunc.open('https://boards.greenhouse.io/tribalcredit')
    context.helperfunc.maximize()


@when("I perform the Departments filter option for (?P<Departments>.+)")
def step_impl(context, Departments):
    list_all_sections = context.helperfunc.find_elements_by_tag_name("section")
    time.sleep(1)
    for item in list_all_sections:
        header_section = item.find_element_by_tag_name("h3")
        if header_section.text == Departments:
            department_id = header_section.get_attribute('id')
            context.selected_department = department_id

    context.helperfunc.find_by_id("s2id_departments-select").click()
    context.helperfunc.find_by_id("select2-result-label-9").click()
    time.sleep(3)


@step('I perform the Offices filter option for (?P<Offices>.+)')
def step_impl(context, Offices):
    context.helperfunc.find_by_id("s2id_offices-select").click()
    context.helperfunc.find_by_id("select2-result-label-25").click()
    time.sleep(3)


@then('I should see in the Job List the (?P<job_positions>.+) result displayed')
def step_impl(context, job_positions):
    context_value = context.selected_department
    elements = context.helperfunc.find_elements_by_xpath("//div[@department_id=" + context_value + "]/a")
    time.sleep(2)

    list = []
    for item in elements:
        list.append(item.text)
    assert job_positions in set(list), f'The job position: "{job_positions}" does not exists.'


@when("I check all the Departments options")
def step_impl(context):
    context.helperfunc.find_by_id("s2id_departments-select").click()


@then("I should see all the (?P<Departments>.+) correctly displayed")
def step_impl(context, Departments):
    print(f"Finding the Department: {Departments}")

@then("I should see all the company (?P<Departments>.+)")
def step_impl(context, Departments):
    print("Falta implementar")


@step("each option can be selected (?P<id>.+)")
def step_impl(context, id):
    time.sleep(1)
    context.helperfunc.find_by_id(id).click()
    attribute = context.helperfunc.find_by_id(id).get_attribute("class")
    print(f"El atributo es: {attribute}")


@when("I check all the filtering options from (?P<Offices>.+)")
def step_impl(context, Offices):
    print(f"Finding the Department: {Offices}")

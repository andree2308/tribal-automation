Feature: The user finds current job openings on the Tribal Credit Website.
  For this purpose the user should interact with the filter options for Departments and Offices

  @test-01
  Scenario Outline: Look for a "Senior QA Engineer" in the Engineering Department in Mexico
    Given I am on the Tribal Credit Website
    When I perform the Departments filter option for <Departments>
    And I perform the Offices filter option for <Offices>
    Then I should see in the Job List the <job_position> result displayed
    Examples:
      | Departments | Offices | job_position       |
      | Engineering | Mexico  | Sr. QA Engineer    |


  @test-02
  Scenario Outline: Validate all the filtering options from the Departments options.
    Given I am on the Tribal Credit Website
    When I check all the Departments options
    Then I should see all the <Departments> correctly displayed
    Examples:
      | Departments                                |
      | All Departments                            |
      | Blockchain                                 |
      | Communications and Culture                 |
      | Credit Success                             |
      | Customer Success                           |
      | Customer Support                           |
      | Data Science and Research                  |
      | Engineering                                |
      | Finance and Accounting                     |
      | Growth                                     |
      | Legal and Compliance                       |
      | Marketing                                  |
      | Operations                                 |
      | People Ops and Talent Acquisition          |
      | Product                                    |
      | Product Marketing                          |
      | Sales and Business Development             |
      | You Don't See a Role You're Interested In? |


  @test-02
  Scenario Outline: Validate all the filtering options from the Offices options.
    Given I am on the Tribal Credit Website
    When I check all the filtering options from <Offices>
    Then I should see all the options correctly displayed
    Examples:
      | Offices       |
      | All Offices   |
      | Brazil        |
      | Chile         |
      | Colombia      |
      | Egypt         |
      | Mexico        |
      | Perú          |
      | United States |